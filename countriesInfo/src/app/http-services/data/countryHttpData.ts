export class CountryHttpData {
  flags: {
    png: string;
  }
  currencies: any;
  capital: string[];
  population: number;
  region: string;

  constructor(flags: { png: string }, currencies: any, capital: string[], population: number, region: string) {
    this.flags = flags;
    this.currencies = currencies;
    this.capital = capital;
    this.population = population;
    this.region = region;
  }

}
