export class ContinentHttpData {
  flags: {
    png: string;
  }
  name: {
    official: string;
  }

  constructor(flags: { png: string }, name: {official: string}) {
    this.flags = flags;
    this.name = name;
  }
}
