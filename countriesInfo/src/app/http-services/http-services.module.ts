import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ContinentHttpService} from "./services/continent-http-service";
import {HttpClientModule} from "@angular/common/http";
import {CountryHttpService} from "./services/country-http-service";



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    ContinentHttpService,
    CountryHttpService
  ]
})
export class HttpServicesModule { }
