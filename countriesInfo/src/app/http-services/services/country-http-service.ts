import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CountryHttpService {

  private readonly address: string = "https://restcountries.com/v3.1/name/";
  constructor(private restClient: HttpClient) {
  }

  getCountryCurrencyFlagCapitalRegionAndPopulation(countryName: string): Promise<any> {
    return this.restClient.get(this.address + countryName + "?fields=currencies,flags,capital,population,region").toPromise();
  }

}
