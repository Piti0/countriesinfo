import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ContinentHttpService {

  private readonly address: string = "https://restcountries.com/v3.1/region/";
  constructor(private restClient: HttpClient) {
  }

  getContinentCountriesNamesAndFlags(continentName: string): Promise<any> {
    return this.restClient.get(this.address + continentName + "?fields=name,flags").toPromise();
  }

}
