import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorInfoComponent } from './author-info/author-info.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [
    AuthorInfoComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule
  ],
  exports: [
    AuthorInfoComponent
  ]
})
export class AuthorInfoViewModule { }
