import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ContinentListComponent} from "./continent-list-view/continent-list/continent-list.component";
import {ContinentInfoComponent} from "./continent-info-view/continent-info/continent-info.component";
import {CountryInfoComponent} from "./country-info-view/country-info/country-info.component";

export const routes: Routes = [
  {path: '', component: ContinentListComponent},
  {path: 'continent/:continentName', component: ContinentInfoComponent},
  {path: 'country/:countryName', component: CountryInfoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
