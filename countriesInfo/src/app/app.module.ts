import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ContinentListViewModule} from "./continent-list-view/continent-list-view.module";
import {ContinentInfoModule} from "./continent-info-view/continent-info.module";
import {CountryInfoViewModule} from "./country-info-view/country-info-view.module";
import {MatButtonModule} from "@angular/material/button";
import {AuthorInfoViewModule} from "./author-info-view/author-info-view.module";
import {MatDialogModule} from "@angular/material/dialog";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ContinentListViewModule,
    ContinentInfoModule,
    CountryInfoViewModule,
    MatButtonModule,
    AuthorInfoViewModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
