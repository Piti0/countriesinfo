import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinentListComponent } from './continent-list/continent-list.component';
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [
    ContinentListComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  exports: [
    ContinentListComponent
  ]
})
export class ContinentListViewModule { }
