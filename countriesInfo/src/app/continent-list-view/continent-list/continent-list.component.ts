import { Component } from '@angular/core';
import {Continent} from "../continent/continent";
import {Router} from "@angular/router";

@Component({
  selector: 'app-continent-list',
  templateUrl: './continent-list.component.html',
  styleUrls: ['./continent-list.component.css']
})

export class ContinentListComponent {

  constructor(private router:Router) {
  }
  readonly continents: Continent[]=[new Continent("Afryka","africa"),
    new Continent("Ameryki","america"),
    new Continent("Azja","asia"),
    new Continent("Europa","europe"),
    new Continent("Oceania","oceania")];

  public chooseContinent(restName: string) {
    this.router.navigateByUrl("continent/" + restName);
  }
}
