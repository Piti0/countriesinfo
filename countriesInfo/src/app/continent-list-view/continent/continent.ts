export class Continent {
  public displayName: string;
  public restName: string;

  constructor(displayName: string, restName: string) {
    this.displayName = displayName;
    this.restName = restName;
  }
}
