import { Component } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {AuthorInfoComponent} from "./author-info-view/author-info/author-info.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'countriesInfo';

  constructor(private dialog: MatDialog){}

  openAuthorPopUp() {
    this.dialog.open(AuthorInfoComponent);
  }
}
