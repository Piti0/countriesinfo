import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryInfoComponent } from './country-info/country-info.component';
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [
    CountryInfoComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  exports: [
    CountryInfoComponent
  ]
})
export class CountryInfoViewModule { }
