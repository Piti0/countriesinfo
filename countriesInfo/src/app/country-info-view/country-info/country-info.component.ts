import { Component } from '@angular/core';
import {ContinentHttpData} from "../../http-services/data/continentHttpData";
import {ActivatedRoute, Router} from "@angular/router";
import {ContinentHttpService} from "../../http-services/services/continent-http-service";
import {CountryHttpService} from "../../http-services/services/country-http-service";
import {CountryHttpData} from "../../http-services/data/countryHttpData";

@Component({
  selector: 'app-country-info',
  templateUrl: './country-info.component.html',
  styleUrls: ['./country-info.component.css']
})
export class CountryInfoComponent {

  public countryName: string | null = null;
  public countryHttpData: CountryHttpData | null = null;

  constructor(private router:Router, private route: ActivatedRoute, private countryHttpService: CountryHttpService){}

  ngOnInit(){
    this.countryName = this.route.snapshot.paramMap.get('countryName');
    this.countryHttpService.getCountryCurrencyFlagCapitalRegionAndPopulation(this.countryName as string)
      .then((value: CountryHttpData[]) => {
        this.countryHttpData = value[0];
        console.log(this.countryHttpData);
      }).catch(() => {
      console.log("Error loading info about country:" + this.countryName);
    });
  }

  getBack() {
    this.router.navigateByUrl("continent/" + this.countryHttpData?.region);
  }

  getCurrency(): string[] {
    return Object.keys(this.countryHttpData?.currencies);
  }

}
