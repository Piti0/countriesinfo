import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinentInfoComponent } from './continent-info/continent-info.component';
import {HttpServicesModule} from "../http-services/http-services.module";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [
    ContinentInfoComponent
  ],
  imports: [
    CommonModule,
    HttpServicesModule,
    MatTableModule,
    MatButtonModule
  ],
  exports: [
    ContinentInfoComponent
  ]
})
export class ContinentInfoModule { }
