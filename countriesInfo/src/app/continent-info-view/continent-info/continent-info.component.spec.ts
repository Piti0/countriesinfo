import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinentInfoComponent } from './continent-info.component';

describe('ContinentInfoComponent', () => {
  let component: ContinentInfoComponent;
  let fixture: ComponentFixture<ContinentInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContinentInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContinentInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
