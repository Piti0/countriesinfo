import { Component } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ContinentHttpService} from "../../http-services/services/continent-http-service";
import {Continent} from "../../continent-list-view/continent/continent";
import {ContinentHttpData} from "../../http-services/data/continentHttpData";

@Component({
  selector: 'app-continent-info-view',
  templateUrl: './continent-info.component.html',
  styleUrls: ['./continent-info.component.css']
})
export class ContinentInfoComponent {

  public continentName: string | null = null;
  public continentHttpData: ContinentHttpData[] = [];

  constructor(private router:Router, private route: ActivatedRoute, private continentHttpService: ContinentHttpService){}

  ngOnInit(){
    this.continentName = this.route.snapshot.paramMap.get('continentName');
    this.continentHttpService.getContinentCountriesNamesAndFlags(this.continentName as string)
      .then((value: ContinentHttpData[]) => {
        this.continentHttpData = value;
      }).catch(() => {
      console.log("Error loading info about continent:" + this.continentName);
    });
  }

  getBack() {
    this.router.navigateByUrl("");
  }

  getDetails(countryName: string) {
    this.router.navigateByUrl("country/" + countryName);
  }

}
